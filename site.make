core = 7.x
api = 2

; uw_ct_archive_page
projects[uw_ct_archive_page][type] = "module"
projects[uw_ct_archive_page][download][type] = "git"
projects[uw_ct_archive_page][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_archive_page.git"
projects[uw_ct_archive_page][download][tag] = "7.x-1.5"
projects[uw_ct_archive_page][subdir] = ""

